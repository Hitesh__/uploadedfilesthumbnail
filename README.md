# View uploaded files as thumbnails

![thumbnail_1.png](https://bitbucket.org/repo/g5q94Xz/images/50213320-thumbnail_1.png)
## Use Case

User wanted to see all the uploaded files as thumbnails and wants some basic features like file preview, file download and file
deletion.

## Functional Details

This asset is used to view uploaded files as thumbnails. User can preview files, download them and can delete them as well.

## Technical Details

This component is built for internal salesforce application, cannot be used for salesforce communities.
You have to place the component on a record page in salesforce.

API Params -> 
@recordId

It automatically gets the recordId of the record on which this component is placed. It will fetch all the uploaded documents associated
to that record and displays them.
